#!/usr/bin/python3

# https://matrix.org/docs/guides/client-server-api

import requests
import random
import time

# Enter the url
host  = "https://matrix.example.com"

# Create some rooms manually and use the room_id code
rooms = [
  'TBwCnzlALWXuAmjhdG:matrix.example.com',
  'OtyWcKGHjQjHhxrYsO:matrix.example.com',
  'zVVwzENIxSswhJUfPA:matrix.example.com'
]

####################################################
# Some default settings. (dutch chat bots)
####################################################

users = [
  "jan",
  "piet",
  "klaas",
  "karel",
  "linda",
  "joep",
  "petra",
  "joost",
  "hendirk",
  "anne",
  "marieke",
  "gerrit"
]

messages = [
  'Hallo allemaal!',
  'Hallo hier ben ik!',
  'Nou, met mij gaat het prima.',
  'Was jij daar ook mee bezig?',
  'Het komt helemaal goed',
  'Valt er hier nog wat te beleven?',
  'Waar was jij nou?',
  'Het weer is vandaag prachtig',
  'Hoeveel besmettingen nu weer?',
  'Ja, daaaag!',
  'Meen je dat nou?',
  'Ik heb trek in een biertje',
  'Met jou erbij is het altijd gezellig! :-)',
  'Ik ga er zo mee stoppen',
  'Vanaf nu doe ik niet meer mee!',
  'Beter vandaag dan morgen',
  'Komen jullie ook zaterdag avond?',
  'Een dag niet gelachen is een dag niet geleefd!',
  'Jullie zijn altijd welkom hoor.'
]

####################################################

requests.packages.urllib3.disable_warnings()

class matrix:
    
    def __init__(self, bots, messages, rooms):
    
        self.bots     = bots
        self.rooms    = rooms
        self.messages = messages
        self.tokens   = {}
        self.password = 'verysecret'
        self.action   = {
            'login': '_matrix/client/r0/login',
            'reg':   '_matrix/client/r0/register',
            'msg':   ['_matrix/client/r0/rooms/%21', '/send/m.room.message?access_token=' ],
            'join':  ['_matrix/client/r0/rooms/%21', '/join?access_token=']
        }

    def get_message(self):
        rand_msg = self.messages[random.randrange(0,len(self.messages))]
        return rand_msg

    def get_bot(self):
        rand_bot = self.bots[random.randrange(0,len(self.bots ))]
        rand_bot = rand_bot + str(random.randrange(1,10))
        return rand_bot

    def get_room(self):
        rand_room = self.rooms[random.randrange(0, len(self.rooms))]
        return rand_room

    def join_room(self, user, room):
        url = host + '/' + self.action['join'][0] + room + self.action['join'][1] + self.tokens[user]
        res = requests.post(url, verify=False)
        print(res.text)

    def send_message(self, user, room, text):
        url = host + '/' + self.action['msg'][0] + room + self.action['msg'][1] + self.tokens[user]
        obj = {
            "msgtype":" m.text",
            "body": text
        }
        res = requests.post(url, json = obj, verify=False)
        print(res.text)
        return res.json()

    def user_login(self, user):
        url = host + '/' + self.action['login']
        obj = {
            "user": user,
            "password": self.password,
            "type": "m.login.password"
        }
        res = requests.post(url, json = obj, verify=False)
        self.tokens[user] = res.json()['access_token']
        return res.json()

    def user_register(self, user):
        url = host + '/' + self.action['reg']
        obj = {
            "username": user,
            "password": self.password,
            "auth": {"type":"m.login.dummy"}
        }
        res = requests.post(url, json = obj, verify=False)
        print(res.text)
        self.tokens[user] = res.json()['access_token']
        return res.json()


t = matrix(users, messages, rooms)

while 1:
    
    rand_user = t.get_bot()
    rand_room = t.get_room()
    rand_text = t.get_message()
    
    print('-> ' + rand_user + ', ' + rand_room + ', ' + rand_text)
    
    if not t.tokens.get(rand_user):

        try:
            t.user_login(rand_user)
        except:
            t.user_register(rand_user)

        time.sleep(10)

    t.join_room(rand_user, rand_room)
    t.send_message(rand_user, rand_room, rand_text)
    time.sleep(2)


