#!/bin/bash

while true; do

  printf "Checking daemon.."
  if [[ `ps aux | grep 'turnserver' \
        | wc -l` -le 1 ]]; then

    echo "[Down]"
    printf "Starting daemon..."
    /etc/init.d/coturn start
  fi

  echo "[Ok]"
  sleep 10
done

