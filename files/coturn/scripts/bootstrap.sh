#!/bin/bash

###################
apt-get update
apt-get -y upgrade
apt-get -y install \
  procps           \
  coturn
###################
cp -v /var/tmp/turnserver.conf /etc/turnserver.conf
chmod 640 /etc/turnserver.conf
chown root.turnserver /etc/turnserver.conf
echo 'TURNSERVER_ENABLED=1' >> /etc/default/coturn

