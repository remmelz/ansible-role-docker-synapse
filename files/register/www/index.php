<?php

include 'config.php';

function newMatrixUser($user, $pass, $host, $key)
{
    $cmd = 'register_new_matrix_user';
    $arg = '--no-admin';
    $result = exec("$cmd $arg -u $user -p '$pass' -k '$key' http://$host:8008");
    return $result;
}

function inviteCounter($invite_file)
{
    $handle = fopen($invite_file, 'r');
    $count = fread($handle, filesize($invite_file));
    $count = intval($count);
    $count--;
    fclose($handle);

    if ($count <= 0) {
        unlink($invite_file);
    } else {
        $handle = fopen($invite_file, 'w');
        fwrite($handle, strval($count) . "\n");
        fclose($handle);
    }
}

function readAsString(string $fname): string
{
    $myfile = fopen($fname, "r") or die("Unable to open file!");
    $content = fread($myfile, filesize($fname));
    fclose($myfile);
    return $content;
}

$html = readAsString("templates/index.tpl");
$form = readAsString("templates/index_form.tpl");
$code = "";
$finalmsg ="";
$errorstate="ok";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $user  = $_POST['user'];
    $pass1 = $_POST['pass1'];
    $pass2 = $_POST['pass2'];
    $code  = $_POST['code'];

    $user = strtolower($user);
    $code = strtolower($code);
    $error = 0;
    $errormsg = "";
    $successmsg = $successmsg . "Welkom $user!!<br>";
    $successmsg = $successmsg . "Je account is aangemaakt.<br>";
    $successmsg = $successmsg . "Je kunt inloggen via <a href='https://matrix.to/#/#public:$matrix_dom'>Matrix</a></p>";

    // check input
    if (preg_match("#^[a-zA-Z0-9]+$#", $code)) {
        $codefile = "/srv/www/invites/$code";
    } else {
        $codefile = "/none";
        $error++;
    }

    if ($pass1 !== $pass2) {
        $errormsg = $errormsg . 'Wachtwoorden zijn niet gelijk.<br>';
        $error++;
    }

    if (strpos($pass1, "'") !== false) {
        $errormsg = $errormsg . "Gebruik geen (') in je wachtwoord.<br>";
        $error++;
    }

    if (strlen($pass1) < 6) {
        $errormsg = $errormsg . 'Kies een wachtwoord langer dan 6.<br>';
        $error++;
    }

    if (!preg_match('/^\w{3,}$/', $user)) {
        $errormsg = $errormsg . "Gebruikersnaam zonder spaties en 3 of meer karakters.<br>";
        $error++;
    }

    if (!file_exists($codefile)) {
        sleep(2); //bruteforce penalty
        $errormsg = $errormsg . 'Geen geldige ticket code.<br>';
        $error++;
    }

    // input ok? 
    if ($error == 0) { 
      // attempt to create the matrix user
      $result = newMatrixUser($user, $pass1, $matrix_host, $matrix_secret);
      if (strpos($result, 'Success') !== false) {
          inviteCounter($codefile);
      } else {
          $errormsg = $errormsg . $result . "<br>";
          $error++;
      }
    }

    // matrix user created succesfully?
    if ($error > 0) { 
      //Errors: Restore the form values
      $form = str_replace("@username@",$user,$form);
      $form = str_replace("@pass1@",$pass1,$form);
      $form = str_replace("@pass2@",$pass2,$form);
      $form = str_replace("@ticketcode@",$code,$form);
      $form = str_replace("@username@",$user,$form);
      $errorstate="error";
      $finalmsg = $errormsg;
    } else {
      $finalmsg = $successmsg;
      $errorstate="success";
      $form="";
    }
}
else { // 1e page load
    if (isset($_GET['code'])) {
      $code = trim($_GET['code']);
      }
    //set ticketcode
    $form = str_replace("@ticketcode@",$code,$form);
    //empty values for the rest
    $form = str_replace("@username@","",$form);
    $form = str_replace("@pass1@","",$form);
    $form = str_replace("@pass2@","",$form);
    $form = str_replace("@username@","",$form);
}

//final replace
$html = str_replace('@errorstate@', $errorstate, $html);
$html = str_replace('@finalmsg@', $finalmsg, $html);
$html = str_replace('@domain@', $matrix_dom, $html);

$body = $form;
$body = $body . "<a href='$matrix_help'>";
$body = $body . '<img src="help.svg" height=24px align="right"></a>'."\n";
$body = $body . "<a href='invite.php'>";
$body = $body . '<img src="key.svg" height=24px align="right"></a>'."\n";
$body = $body . '</div>';

$html = str_replace('@body@', $body, $html);

echo $html; 

