<?php

session_start();

include 'config.php';


function readAsString(string $fname): string
{
    $myfile = fopen($fname, "r") or die("Unable to open file!");
    $content = fread($myfile, filesize($fname));
    fclose($myfile);
    return $content;
}

function getInviteCode()
{
    $list_invites = scandir('/srv/www/invites/');
    $list_invites = array_diff($list_invites, array('..','.'));
    $invite = $list_invites[rand(2, sizeof($list_invites))];
    $invite = strtoupper($invite);

    return $invite;
}


$html = readAsString("templates/invite.tpl");

$body = '';
$errormsg = 'ok';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($_POST['pass']) {

        $_SESSION['pass'] = $_POST['pass'];
    }

    if ($_SESSION['pass'] == $site_passwd) {

        $form = readAsString("templates/invite_form.tpl");
        $form = str_replace('@invite_code@', getInviteCode(), $form);
        $body = $form;

    } else {
        $errormsg = '<Fout: geen geldig wachtwoord.';
        $body = $errormsg;
    }

} else {
  $body = $body . '<form action="invite.php" method="post">'."\n";
  $body = $body . '<p><label for="pass">Password:</label></p>'."\n";
  $body = $body . '<input type="password" name="pass"><br>'."\n";
}

$body = str_replace('@domain@', $matrix_dom, $body);
$html = str_replace('@body@', $body, $html);

echo $html;

?>

