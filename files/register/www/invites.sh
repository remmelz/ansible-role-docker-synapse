#!/bin/bash

num=1500

invites="/srv/www/invites"
wwwusr="www-data"

if [[ ! -d ${invites} ]]; then
  mkdir -p ${invites}
fi

cd ${invites}

c=0
i=`ls -1 | wc -l`
while [[ ${i} -le ${num} ]]; do

  code=`pwgen -0 -N1 -v | tr [A-Z] [a-z]`

  echo 1 > ${code}
  chown ${wwwusr}. ${code}

  let i=${i}+1
  let c=${c}+1
  
done

let c=${c}-1
if [[ ${c} -gt 0 ]]; then
  echo "${c} invites generated."
fi


