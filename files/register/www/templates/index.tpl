<!DOCTYPE html>
<html>
<head>
  <title>Register</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
<img src="chat.svg" height=64px align="right">

<h2>Registreren</h2>
<p>Aanmelden voor @domain@</p>
<p class="@errorstate@">
  @finalmsg@
</p>

@body@

</body>
</html>
