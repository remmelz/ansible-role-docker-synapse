#!/bin/bash

service php7.4-fpm start
service nginx start

chown -R www-data. /srv/www

sleep 1

while true; do

  service nginx status
  
  # Generate invites codes
  /srv/www/invites.sh
  
  sleep 10

done

