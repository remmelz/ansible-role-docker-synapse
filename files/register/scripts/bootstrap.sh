#!/bin/bash

###################
apt-get update
apt-get -y upgrade
apt-get -y install \
  curl             \
  python3-pip      \
  pwgen            \
  nginx            \
  php              \
  php-gd           \
  php-fpm          \
  php-xml &&       \
pip install matrix-synapse
###################
mkdir -p /var/run/php
chown www-data. /var/run/php
cd /var/run/php
ln -s php7.4-fpm.sock php-fpm.sock
###################
cd /srv
mv -v /var/tmp/www .
chown -R www-data. .
chmod a+x ./www/invites.sh
###################
mv -v /var/tmp/nginx.conf /etc/nginx/sites-available/register.conf
cd /etc/nginx/sites-enabled
rm default
ln -s ../sites-available/register.conf

