
Ansible Role Docker Synapse
============================

Matrix is really a decentralised conversation store rather than a messaging 
protocol. When you send a message in Matrix, it is replicated over all the 
servers whose users are participating in a given conversation - similarly 
to how commits are replicated between Git repositories.

* https://matrix.org/


Requirements
------------

Tested with Debian GNU/Linux (Buster) with docker.


Dependencies
------------

* ansible-role-docker-nginx


License
-------

See license.md



