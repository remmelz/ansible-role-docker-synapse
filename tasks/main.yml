---

- name: Shutdown and removing related containers
  docker_container:
    #container_default_behavior: "compatibility"
    name: '{{ project }}_{{ item }}_{{ index }}'
    state: absent
  with_items:
    - "{{ service }}"
    - "postgres"
    - "register"
    - "coturn"

- name: Pull required Docker images
  shell: "docker pull {{ item }}"
  with_items:
    - "matrixdotorg/synapse:latest"
    - "debian:testing"

- name: Ensure cache directory is removed
  file:
    path: '{{ compose_dir }}/{{ project }}_{{ service }}'
    state: absent

- name: Creating cache compose directory
  file:
    path: '{{ compose_dir }}/{{ project }}_{{ service }}'
    state: directory
    recurse: yes

- name: Preparing cache compose directory
  file:
    path: '{{ compose_dir }}/{{ project }}_{{ service }}/{{ item }}'
    state: directory
  with_items:
    - 'synapse'
    - 'register'
    - 'coturn'

- name: Retrieve docker gateway address
  shell: |
    docker network inspect {{ network }} \
     | grep -i gateway | awk -F'"' '{ print $4 }'
  register: docker_gateway_address

- name: Copying docker build files
  template:
    src: 'templates/docker/{{ item }}'
    dest: '{{ compose_dir }}/{{ project }}_{{ service }}/{{ item }}'
  with_items:
    - 'docker-compose.yml'
    - 'coturn/Dockerfile'
    - 'register/Dockerfile'

- name: Copying files
  copy:
    src: '{{ item }}'
    dest: '{{ compose_dir }}/{{ project }}_{{ service }}'
  with_items:
    - 'register'
    - 'coturn'

- name: Copying nginx config file
  template:
    src: 'templates/nginx/{{ item }}'
    dest: '{{ nginx_dir }}/{{ project }}_{{ item }}'
  with_items:
    - 'synapse.conf'

- name: Get deployment status
  shell: >
    docker run --rm \
      --mount type=volume,src={{ project }}_{{ service }}_data,dst=/data \
      debian:testing test -e /data/.deployed
  register: deployed
  ignore_errors: true

- name: Generating secrets
  shell: >
    docker run --rm \
      --mount type=volume,src={{ project }}_{{ service }}_data,dst=/data \
      -e SYNAPSE_SERVER_NAME={{ domain }} \
      -e SYNAPSE_REPORT_STATS=no \
      matrixdotorg/synapse:latest generate
  when: deployed.rc == 1
      
- name: Retrieve macaroon secret
  shell: >
    docker run --rm --mount type=volume,src={{ project }}_{{ service }}_data,dst=/data debian:testing \
      grep "^macaroon_secret_key:" /data/homeserver.yaml | awk -F'"' '{print $2}'
  register: macaroon_secret_key

- name: Retrieve form secret
  shell: >
    docker run --rm --mount type=volume,src={{ project }}_{{ service }}_data,dst=/data debian:testing \
      grep "^form_secret:" /data/homeserver.yaml | awk -F'"' '{print $2}'
  register: form_secret

- name: Copying config files
  template:
    src: 'templates/{{ item }}'
    dest: '{{ compose_dir }}/{{ project }}_{{ service }}/{{ item }}'
  with_items:
    - 'synapse/homeserver.yaml'
    - 'synapse/log.config'
    - 'coturn/turnserver.conf'

- name: Copying register config files
  template:
    src: 'templates/register/{{ item }}'
    dest: '{{ compose_dir }}/{{ project }}_{{ service }}/register/www/{{ item }}'
  with_items:
    - 'config.php'
 
- name: Building the containers
  shell: >
    cd {{ compose_dir }}/{{ project }}_{{ service }} &&
    docker-compose -p {{ project }} up --build --no-start

- name: Copy config files to container
  shell: |
    cd {{ compose_dir }}/{{ project }}_{{ service }}
    echo ok > deployed
    docker cp deployed {{ project }}_{{ service }}_{{ index }}:/data/.deployed
    docker cp ./synapse/homeserver.yaml {{ project }}_{{ service }}_{{ index }}:/data/homeserver.yaml
    docker cp ./synapse/log.config {{ project }}_{{ service }}_{{ index }}:/data/{{ domain }}.log.config
  when: deployed.rc == 1

